﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace JDKill.UIs.Common
{
    public partial class PanelBase : UserControl
    {
        public PanelBase()
        {
            InitializeComponent();
        }

        protected Thread RunTaskThread(Action action)
        {
            return this.RunTaskThread(action, ApartmentState.MTA);
        }

        protected Thread RunTaskThread(Action action, ApartmentState state)
        {
            Thread thread = new Thread(() => action());
            thread.SetApartmentState(state);
            thread.IsBackground = true;
            thread.Start();
            return thread;
        }

        protected void RunTaskInvoke(Action action)
        {
            if (base.InvokeRequired)
            {
                base.Invoke(action);
            }
            else
            {
                action();
            }
        }

        private void PanelBase_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
        }
    }
}
