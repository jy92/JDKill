﻿namespace JDKill.UIs.Setting
{
    partial class PanelProductKillSetting
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxAutoCreateOrder = new System.Windows.Forms.CheckBox();
            this.checkBoxCustomQueryInternal = new System.Windows.Forms.CheckBox();
            this.numericUpDownQueryInternalMin = new System.Windows.Forms.NumericUpDown();
            this.buttonSelectAddress = new System.Windows.Forms.Button();
            this.textBoxCurAddress = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.listBoxCurProductList = new System.Windows.Forms.ListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxMakeOrderCondition = new System.Windows.Forms.ComboBox();
            this.textBoxConditionProductValue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownQueryInternalMax = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQueryInternalMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQueryInternalMax)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBoxAutoCreateOrder
            // 
            this.checkBoxAutoCreateOrder.AutoSize = true;
            this.checkBoxAutoCreateOrder.Checked = true;
            this.checkBoxAutoCreateOrder.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAutoCreateOrder.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBoxAutoCreateOrder.Location = new System.Drawing.Point(462, 79);
            this.checkBoxAutoCreateOrder.Name = "checkBoxAutoCreateOrder";
            this.checkBoxAutoCreateOrder.Size = new System.Drawing.Size(72, 16);
            this.checkBoxAutoCreateOrder.TabIndex = 18;
            this.checkBoxAutoCreateOrder.Text = "自动下单";
            this.checkBoxAutoCreateOrder.UseVisualStyleBackColor = true;
            // 
            // checkBoxCustomQueryInternal
            // 
            this.checkBoxCustomQueryInternal.AutoSize = true;
            this.checkBoxCustomQueryInternal.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBoxCustomQueryInternal.Location = new System.Drawing.Point(276, 55);
            this.checkBoxCustomQueryInternal.Name = "checkBoxCustomQueryInternal";
            this.checkBoxCustomQueryInternal.Size = new System.Drawing.Size(156, 16);
            this.checkBoxCustomQueryInternal.TabIndex = 17;
            this.checkBoxCustomQueryInternal.Text = "自定义查询间隔(毫秒)：";
            this.checkBoxCustomQueryInternal.UseVisualStyleBackColor = true;
            this.checkBoxCustomQueryInternal.CheckedChanged += new System.EventHandler(this.checkBoxCustomQueryInternal_CheckedChanged);
            // 
            // numericUpDownQueryInternalMin
            // 
            this.numericUpDownQueryInternalMin.Enabled = false;
            this.numericUpDownQueryInternalMin.Location = new System.Drawing.Point(277, 77);
            this.numericUpDownQueryInternalMin.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.numericUpDownQueryInternalMin.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownQueryInternalMin.Name = "numericUpDownQueryInternalMin";
            this.numericUpDownQueryInternalMin.Size = new System.Drawing.Size(80, 21);
            this.numericUpDownQueryInternalMin.TabIndex = 16;
            this.numericUpDownQueryInternalMin.Value = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.numericUpDownQueryInternalMin.ValueChanged += new System.EventHandler(this.numericUpDownQueryInternalMin_ValueChanged);
            // 
            // buttonSelectAddress
            // 
            this.buttonSelectAddress.Location = new System.Drawing.Point(521, 24);
            this.buttonSelectAddress.Name = "buttonSelectAddress";
            this.buttonSelectAddress.Size = new System.Drawing.Size(32, 23);
            this.buttonSelectAddress.TabIndex = 15;
            this.buttonSelectAddress.Text = "选";
            this.buttonSelectAddress.UseVisualStyleBackColor = true;
            this.buttonSelectAddress.Click += new System.EventHandler(this.buttonSelectAddress_Click);
            // 
            // textBoxCurAddress
            // 
            this.textBoxCurAddress.BackColor = System.Drawing.Color.White;
            this.textBoxCurAddress.Location = new System.Drawing.Point(275, 25);
            this.textBoxCurAddress.Name = "textBoxCurAddress";
            this.textBoxCurAddress.ReadOnly = true;
            this.textBoxCurAddress.Size = new System.Drawing.Size(240, 21);
            this.textBoxCurAddress.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(275, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "收货地址：";
            // 
            // listBoxCurProductList
            // 
            this.listBoxCurProductList.FormattingEnabled = true;
            this.listBoxCurProductList.ItemHeight = 12;
            this.listBoxCurProductList.Location = new System.Drawing.Point(8, 28);
            this.listBoxCurProductList.Name = "listBoxCurProductList";
            this.listBoxCurProductList.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBoxCurProductList.Size = new System.Drawing.Size(244, 124);
            this.listBoxCurProductList.TabIndex = 12;
            this.listBoxCurProductList.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listBoxCurProductList_MouseUp);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 11;
            this.label12.Text = "抢购列表：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(277, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 19;
            this.label1.Text = "下单条件：";
            // 
            // comboBoxMakeOrderCondition
            // 
            this.comboBoxMakeOrderCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMakeOrderCondition.FormattingEnabled = true;
            this.comboBoxMakeOrderCondition.Location = new System.Drawing.Point(276, 126);
            this.comboBoxMakeOrderCondition.Name = "comboBoxMakeOrderCondition";
            this.comboBoxMakeOrderCondition.Size = new System.Drawing.Size(180, 20);
            this.comboBoxMakeOrderCondition.TabIndex = 20;
            this.comboBoxMakeOrderCondition.SelectedIndexChanged += new System.EventHandler(this.comboBoxMakeOrderCondition_SelectedIndexChanged);
            // 
            // textBoxConditionProductValue
            // 
            this.textBoxConditionProductValue.Location = new System.Drawing.Point(462, 125);
            this.textBoxConditionProductValue.Name = "textBoxConditionProductValue";
            this.textBoxConditionProductValue.Size = new System.Drawing.Size(72, 21);
            this.textBoxConditionProductValue.TabIndex = 21;
            this.textBoxConditionProductValue.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(541, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 12);
            this.label3.TabIndex = 22;
            this.label3.Text = "元";
            this.label3.Visible = false;
            // 
            // numericUpDownQueryInternalMax
            // 
            this.numericUpDownQueryInternalMax.Enabled = false;
            this.numericUpDownQueryInternalMax.Location = new System.Drawing.Point(376, 77);
            this.numericUpDownQueryInternalMax.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.numericUpDownQueryInternalMax.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownQueryInternalMax.Name = "numericUpDownQueryInternalMax";
            this.numericUpDownQueryInternalMax.Size = new System.Drawing.Size(80, 21);
            this.numericUpDownQueryInternalMax.TabIndex = 23;
            this.numericUpDownQueryInternalMax.Value = new decimal(new int[] {
            230,
            0,
            0,
            0});
            this.numericUpDownQueryInternalMax.ValueChanged += new System.EventHandler(this.numericUpDownQueryInternalMax_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(360, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 24;
            this.label4.Text = "-";
            // 
            // PanelProductKillSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numericUpDownQueryInternalMax);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxConditionProductValue);
            this.Controls.Add(this.comboBoxMakeOrderCondition);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBoxAutoCreateOrder);
            this.Controls.Add(this.checkBoxCustomQueryInternal);
            this.Controls.Add(this.numericUpDownQueryInternalMin);
            this.Controls.Add(this.buttonSelectAddress);
            this.Controls.Add(this.textBoxCurAddress);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBoxCurProductList);
            this.Controls.Add(this.label12);
            this.Name = "PanelProductKillSetting";
            this.Size = new System.Drawing.Size(1027, 542);
            this.Load += new System.EventHandler(this.PanelProductKillSetting_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQueryInternalMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQueryInternalMax)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxAutoCreateOrder;
        private System.Windows.Forms.CheckBox checkBoxCustomQueryInternal;
        private System.Windows.Forms.NumericUpDown numericUpDownQueryInternalMin;
        private System.Windows.Forms.Button buttonSelectAddress;
        private System.Windows.Forms.TextBox textBoxCurAddress;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBoxCurProductList;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxMakeOrderCondition;
        private System.Windows.Forms.TextBox textBoxConditionProductValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownQueryInternalMax;
        private System.Windows.Forms.Label label4;
    }
}
