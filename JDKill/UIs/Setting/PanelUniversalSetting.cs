﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JDKill.UIs.Common;

namespace JDKill.UIs.Setting
{
    public partial class PanelUniversalSetting : PanelBase
    {
        /// <summary>
        /// 启用定时抢购
        /// </summary>
        public event EventHandler OnTimerKillEnabled;
        /// <summary>
        /// 禁用定时抢购
        /// </summary>
        public event EventHandler OnTimerKillDisabled;
        /// <summary>
        /// 定时抢购触发事件
        /// </summary>
        public event EventHandler OnTimerKillTrigger;
        /// <summary>
        /// 获取或设置定时器时间
        /// </summary>
        public DateTime KillTimer
        {
            get { return this.dateTimePickerKillTime.Value; }
            set { this.dateTimePickerKillTime.Value = value; }
        }
        
        public PanelUniversalSetting()
        {
            InitializeComponent();
            this.colorBlock2.BackColor = GlobalContext.OnSaleColor;
            this.timer1.Tick += new EventHandler(timer1_Tick);
            DateTime now = DateTime.Now;
            DateTime defaultKillDate = new DateTime(now.Year,now.Month,now.Day,now.Hour,59,59);
            this.dateTimePickerKillTime.Value = defaultKillDate; 
        }

        void timer1_Tick(object sender, EventArgs e)
        {
            if (this.dateTimePickerKillTime.Value <= DateTime.Now)
            {
                this.timer1.Enabled = false;
                if (this.OnTimerKillTrigger != null)
                {
                    this.OnTimerKillTrigger(this, EventArgs.Empty);
                }
            }
        }

        private void colorBlock2_BackColorChanged(object sender, EventArgs e)
        {
            GlobalContext.OnSaleColor = this.colorBlock2.BackColor;
        }

        /// <summary>
        /// 开启或禁用定时抢购
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxEnableTimerKill_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBoxEnableTimerKill.Checked)
            {
                DoEnableTimerKill();
            }
            else
            {
                DoDisableTimerKill();
            }
        }

        public void DoEnableTimerKill()
        {
            if (this.checkBoxEnableTimerKill.Checked == false)
            {
                this.checkBoxEnableTimerKill.Checked = true;
            }
            this.dateTimePickerKillTime.Enabled = false;
            if (this.OnTimerKillEnabled != null)
            {
                this.OnTimerKillEnabled(this, EventArgs.Empty);
            }
        }

        public void DoDisableTimerKill()
        {
            if (this.checkBoxEnableTimerKill.Checked == true)
            {
                this.checkBoxEnableTimerKill.Checked = false;
            }
            this.dateTimePickerKillTime.Enabled = true;
            if (this.OnTimerKillDisabled != null)
            {
                this.OnTimerKillDisabled(this, EventArgs.Empty);
            }
        }

        public void StartChecker()
        {
            this.timer1.Interval = 200;
            this.timer1.Enabled = true;
        }

        public void StopChecker()
        {
            this.timer1.Enabled = false;
        }
    }
}
