﻿namespace JDKill.UIs.Setting
{
    partial class PanelQQNotifierSetting
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonCtrlEnter = new System.Windows.Forms.RadioButton();
            this.radioButtonEnter = new System.Windows.Forms.RadioButton();
            this.buttonTestSendQQMsg = new System.Windows.Forms.Button();
            this.buttonGetQQWindow = new System.Windows.Forms.Button();
            this.comboBoxQQWindow = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // radioButtonCtrlEnter
            // 
            this.radioButtonCtrlEnter.AutoSize = true;
            this.radioButtonCtrlEnter.Checked = true;
            this.radioButtonCtrlEnter.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radioButtonCtrlEnter.Location = new System.Drawing.Point(99, 113);
            this.radioButtonCtrlEnter.Name = "radioButtonCtrlEnter";
            this.radioButtonCtrlEnter.Size = new System.Drawing.Size(83, 16);
            this.radioButtonCtrlEnter.TabIndex = 9;
            this.radioButtonCtrlEnter.TabStop = true;
            this.radioButtonCtrlEnter.Text = "Ctrl+Enter";
            this.radioButtonCtrlEnter.UseVisualStyleBackColor = true;
            // 
            // radioButtonEnter
            // 
            this.radioButtonEnter.AutoSize = true;
            this.radioButtonEnter.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radioButtonEnter.Location = new System.Drawing.Point(23, 113);
            this.radioButtonEnter.Name = "radioButtonEnter";
            this.radioButtonEnter.Size = new System.Drawing.Size(53, 16);
            this.radioButtonEnter.TabIndex = 10;
            this.radioButtonEnter.Text = "Enter";
            this.radioButtonEnter.UseVisualStyleBackColor = true;
            // 
            // buttonTestSendQQMsg
            // 
            this.buttonTestSendQQMsg.Location = new System.Drawing.Point(271, 74);
            this.buttonTestSendQQMsg.Name = "buttonTestSendQQMsg";
            this.buttonTestSendQQMsg.Size = new System.Drawing.Size(138, 23);
            this.buttonTestSendQQMsg.TabIndex = 8;
            this.buttonTestSendQQMsg.Text = "测试";
            this.buttonTestSendQQMsg.UseVisualStyleBackColor = true;
            this.buttonTestSendQQMsg.Click += new System.EventHandler(this.buttonTestSendQQMsg_Click);
            // 
            // buttonGetQQWindow
            // 
            this.buttonGetQQWindow.Location = new System.Drawing.Point(271, 45);
            this.buttonGetQQWindow.Name = "buttonGetQQWindow";
            this.buttonGetQQWindow.Size = new System.Drawing.Size(138, 23);
            this.buttonGetQQWindow.TabIndex = 7;
            this.buttonGetQQWindow.Text = "获取聊天窗口";
            this.buttonGetQQWindow.UseVisualStyleBackColor = true;
            this.buttonGetQQWindow.Click += new System.EventHandler(this.buttonGetQQWindow_Click);
            // 
            // comboBoxQQWindow
            // 
            this.comboBoxQQWindow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxQQWindow.FormattingEnabled = true;
            this.comboBoxQQWindow.Location = new System.Drawing.Point(23, 47);
            this.comboBoxQQWindow.Name = "comboBoxQQWindow";
            this.comboBoxQQWindow.Size = new System.Drawing.Size(200, 20);
            this.comboBoxQQWindow.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "选择发送按键:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "选择QQ聊天窗口:";
            // 
            // PanelQQNotifierSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radioButtonCtrlEnter);
            this.Controls.Add(this.radioButtonEnter);
            this.Controls.Add(this.buttonTestSendQQMsg);
            this.Controls.Add(this.buttonGetQQWindow);
            this.Controls.Add(this.comboBoxQQWindow);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Name = "PanelQQNotifierSetting";
            this.Size = new System.Drawing.Size(439, 162);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonCtrlEnter;
        private System.Windows.Forms.RadioButton radioButtonEnter;
        private System.Windows.Forms.Button buttonTestSendQQMsg;
        private System.Windows.Forms.Button buttonGetQQWindow;
        private System.Windows.Forms.ComboBox comboBoxQQWindow;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}
