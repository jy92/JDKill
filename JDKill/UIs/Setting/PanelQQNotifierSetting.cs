﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JDKill.UIs.Common;
using JDKill.Utils;
using JDKill.Definitions;

namespace JDKill.UIs.Setting
{
    public partial class PanelQQNotifierSetting : PanelBase
    {
        private QQNotifier _qqNotify = new QQNotifier();
        public PanelQQNotifierSetting()
        {
            InitializeComponent();
        }

        private void buttonGetQQWindow_Click(object sender, EventArgs e)
        {
            this.comboBoxQQWindow.Items.Clear();
            _qqNotify.FillQQWindowList((wi) =>
            {
                this.comboBoxQQWindow.Items.Add(wi);
            });
        }

        private void buttonTestSendQQMsg_Click(object sender, EventArgs e)
        {
            if (this.comboBoxQQWindow.SelectedItem == null)
            {
                return;
            }

            WindowInfo wi = this.comboBoxQQWindow.SelectedItem as WindowInfo;
            QQSendKeyMode mode = QQSendKeyMode.CtrlEnter;
            if (this.radioButtonEnter.Checked)
            {
                mode = QQSendKeyMode.Enter;
            }
            _qqNotify.SendQQMessage(wi.Hwnd, "测试", mode);
        }

        public void SendQQMessage(string msg)
        {
            WindowInfo wi = this.comboBoxQQWindow.SelectedItem as WindowInfo;
            if (wi != null)
            {
                QQSendKeyMode mode = QQSendKeyMode.CtrlEnter;
                if (this.radioButtonEnter.Checked)
                {
                    mode = QQSendKeyMode.Enter;
                }
                _qqNotify.SendQQMessage(wi.Hwnd, msg, mode);
            }
        }
    }
}
