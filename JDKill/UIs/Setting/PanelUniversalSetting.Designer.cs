﻿namespace JDKill.UIs.Setting
{
    partial class PanelUniversalSetting
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.colorBlock2 = new JDKill.UIs.Common.ColorBlock();
            this.label26 = new System.Windows.Forms.Label();
            this.checkBoxEnableTimerKill = new System.Windows.Forms.CheckBox();
            this.dateTimePickerKillTime = new System.Windows.Forms.DateTimePicker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // colorBlock2
            // 
            this.colorBlock2.BackColor = System.Drawing.Color.Yellow;
            this.colorBlock2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.colorBlock2.Location = new System.Drawing.Point(35, 46);
            this.colorBlock2.Name = "colorBlock2";
            this.colorBlock2.Size = new System.Drawing.Size(131, 23);
            this.colorBlock2.TabIndex = 5;
            this.colorBlock2.BackColorChanged += new System.EventHandler(this.colorBlock2_BackColorChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(33, 25);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(65, 12);
            this.label26.TabIndex = 4;
            this.label26.Text = "可购颜色：";
            // 
            // checkBoxEnableTimerKill
            // 
            this.checkBoxEnableTimerKill.AutoSize = true;
            this.checkBoxEnableTimerKill.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBoxEnableTimerKill.Location = new System.Drawing.Point(35, 85);
            this.checkBoxEnableTimerKill.Name = "checkBoxEnableTimerKill";
            this.checkBoxEnableTimerKill.Size = new System.Drawing.Size(96, 16);
            this.checkBoxEnableTimerKill.TabIndex = 6;
            this.checkBoxEnableTimerKill.Text = "开启定时抢购";
            this.checkBoxEnableTimerKill.UseVisualStyleBackColor = true;
            this.checkBoxEnableTimerKill.CheckedChanged += new System.EventHandler(this.checkBoxEnableTimerKill_CheckedChanged);
            // 
            // dateTimePickerKillTime
            // 
            this.dateTimePickerKillTime.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.dateTimePickerKillTime.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dateTimePickerKillTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerKillTime.Location = new System.Drawing.Point(35, 112);
            this.dateTimePickerKillTime.Name = "dateTimePickerKillTime";
            this.dateTimePickerKillTime.ShowUpDown = true;
            this.dateTimePickerKillTime.Size = new System.Drawing.Size(169, 21);
            this.dateTimePickerKillTime.TabIndex = 7;
            // 
            // PanelUniversalSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dateTimePickerKillTime);
            this.Controls.Add(this.checkBoxEnableTimerKill);
            this.Controls.Add(this.colorBlock2);
            this.Controls.Add(this.label26);
            this.Name = "PanelUniversalSetting";
            this.Size = new System.Drawing.Size(1027, 459);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Common.ColorBlock colorBlock2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox checkBoxEnableTimerKill;
        private System.Windows.Forms.DateTimePicker dateTimePickerKillTime;
        private System.Windows.Forms.Timer timer1;
    }
}
