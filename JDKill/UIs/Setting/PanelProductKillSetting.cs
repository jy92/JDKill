﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JDKill.Definitions;
using JDKill.UIs.Query;
using JDKill.Definitions.Product;
using JDKill.Definitions.Address;
using JDKill.UIs.Common;
using TDLib.Utils;

namespace JDKill.UIs.Setting
{
    public partial class PanelProductKillSetting : PanelBase
    {
        public PanelProductKillSetting()
        {
            InitializeComponent();
            // 初始化下单条件
            foreach (var condition in MakeOrderCondition.AllDefinedTypes)
            {
                this.comboBoxMakeOrderCondition.Items.Add(condition);
            }
            this.comboBoxMakeOrderCondition.SelectedIndex = 0;
        }

        private void buttonSelectAddress_Click(object sender, EventArgs e)
        {
            if (!GlobalContext.IsLogined())
                return;

            var frm = new FrmSelectAddress();
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.textBoxCurAddress.Tag = frm.SelectedAddress;
                this.textBoxCurAddress.Text = frm.SelectedAddress.ToString();
            }
        }

        private void checkBoxCustomQueryInternal_CheckedChanged(object sender, EventArgs e)
        {
            this.numericUpDownQueryInternalMin.Enabled = this.checkBoxCustomQueryInternal.Checked;
            this.numericUpDownQueryInternalMax.Enabled = this.checkBoxCustomQueryInternal.Checked;
        }

        private void listBoxCurProductList_MouseUp(object sender, MouseEventArgs e)
        {
            if (GlobalContext.IsInProductKillProcess)
                return;

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (this.listBoxCurProductList.Items.Count > 0)
                {
                    ContextMenuStrip cms = new ContextMenuStrip();
                    cms.Items.Add("从抢购列表中移除", null, (o, args) =>
                    {
                        var items = this.listBoxCurProductList.SelectedItems;
                        for (int i = 0; i < items.Count; i++)
                        {
                            var item = items[i];
                            this.listBoxCurProductList.Items.Remove(item);
                        }
                    });
                    cms.Show(this.listBoxCurProductList, e.Location);
                }
            }
        }

        public List<ProductDetail> GetProductListInNeed()
        {
            List<ProductDetail> neededProductList = new List<ProductDetail>();
            for (int i = 0; i < listBoxCurProductList.Items.Count; i++)
            {
                var product = listBoxCurProductList.Items[i] as ProductDetail;
                neededProductList.Add(product);
            }

            return neededProductList;
        }

        public void AddToProductListInNeed(ProductDetail product)
        {
            this.listBoxCurProductList.Items.Add(product);
        }

        public AddressDetail GetAddress()
        {
            return this.textBoxCurAddress.Tag as AddressDetail;
        }

        public bool IsAutoCreateOrder
        {
            get { return this.checkBoxAutoCreateOrder.Checked; }
        }

        public bool IsCustomQueryInterval
        {
            get { return this.checkBoxCustomQueryInternal.Checked; }
        }

        public int GetCustomMinQueryInterval()
        {
            return Convert.ToInt32(this.numericUpDownQueryInternalMin.Value);
        }

        public int GetCustomMaxQueryInterval()
        {
            return Convert.ToInt32(this.numericUpDownQueryInternalMax.Value);
        }

        public void SetCustomQueryInterval(int value)
        {
            this.numericUpDownQueryInternalMin.Value = value;
            this.numericUpDownQueryInternalMax.Value = value;
        }

        private void comboBoxMakeOrderCondition_SelectedIndexChanged(object sender, EventArgs e)
        {
            MakeOrderCondition item = this.comboBoxMakeOrderCondition.SelectedItem as MakeOrderCondition;
            if (item != null)
            {
                if (item == MakeOrderCondition.WhenAvailableAndLessEqualCertainPrice)
                {
                    this.label3.Visible = true;
                    this.textBoxConditionProductValue.Visible = true;
                }
                else if (item == MakeOrderCondition.WhenAvailable)
                {
                    this.label3.Visible = false;
                    this.textBoxConditionProductValue.Visible = false;
                }
            }
        }

        /// <summary>
        /// 获取下单条件
        /// </summary>
        /// <returns></returns>
        public MakeOrderCondition GetMakeOrderCondition()
        {
            return this.comboBoxMakeOrderCondition.SelectedItem as MakeOrderCondition;
        }

        /// <summary>
        /// 获取下单时的指定价格
        /// </summary>
        /// <returns></returns>
        public double GetCertainPrice()
        {
            double value;
            if (!double.TryParse(this.textBoxConditionProductValue.Text, out value))
            {
                return -1;
            }
            return value;
        }

        private void numericUpDownQueryInternalMax_ValueChanged(object sender, EventArgs e)
        {
            if (this.numericUpDownQueryInternalMin.Value > this.numericUpDownQueryInternalMax.Value)
            {
                MsgBox.Warning("自定义查询间隔中，最小值必须小于等于最大值！");
                this.numericUpDownQueryInternalMax.Value = this.numericUpDownQueryInternalMin.Value;
            }
        }

        private void numericUpDownQueryInternalMin_ValueChanged(object sender, EventArgs e)
        {
            if (this.numericUpDownQueryInternalMin.Value > this.numericUpDownQueryInternalMax.Value)
            {
                MsgBox.Warning("自定义查询间隔中，最小值必须小于等于最大值！");
                this.numericUpDownQueryInternalMin.Value = this.numericUpDownQueryInternalMax.Value;
            }
        }

        private void PanelProductKillSetting_Load(object sender, EventArgs e)
        {
            this.checkBoxCustomQueryInternal.Checked = true;
        }
    }
}
