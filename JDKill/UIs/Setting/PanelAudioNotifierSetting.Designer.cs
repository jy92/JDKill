﻿namespace JDKill.UIs.Setting
{
    partial class PanelAudioNotifierSetting
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonTestPlay = new System.Windows.Forms.Button();
            this.comboBoxMusicList = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonTestPlay
            // 
            this.buttonTestPlay.Location = new System.Drawing.Point(253, 49);
            this.buttonTestPlay.Name = "buttonTestPlay";
            this.buttonTestPlay.Size = new System.Drawing.Size(138, 23);
            this.buttonTestPlay.TabIndex = 6;
            this.buttonTestPlay.Text = "试听";
            this.buttonTestPlay.UseVisualStyleBackColor = true;
            this.buttonTestPlay.Click += new System.EventHandler(this.buttonTestPlay_Click);
            // 
            // comboBoxMusicList
            // 
            this.comboBoxMusicList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMusicList.FormattingEnabled = true;
            this.comboBoxMusicList.Location = new System.Drawing.Point(25, 51);
            this.comboBoxMusicList.Name = "comboBoxMusicList";
            this.comboBoxMusicList.Size = new System.Drawing.Size(200, 20);
            this.comboBoxMusicList.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "选择声音:";
            // 
            // PanelAudioNotifierSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonTestPlay);
            this.Controls.Add(this.comboBoxMusicList);
            this.Controls.Add(this.label5);
            this.Name = "PanelAudioNotifierSetting";
            this.Size = new System.Drawing.Size(446, 117);
            this.Load += new System.EventHandler(this.PanelAudioNotifierSetting_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonTestPlay;
        private System.Windows.Forms.ComboBox comboBoxMusicList;
        private System.Windows.Forms.Label label5;
    }
}
