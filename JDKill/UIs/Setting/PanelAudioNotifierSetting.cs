﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JDKill.UIs.Common;
using JDKill.Utils;

namespace JDKill.UIs.Setting
{
    public partial class PanelAudioNotifierSetting : PanelBase
    {
        private MusicPlayer _musicPlayer = null;
        public PanelAudioNotifierSetting()
        {
            InitializeComponent();
        }

        private void PanelAudioNotifierSetting_Load(object sender, EventArgs e)
        {
        }

        public void Init(IntPtr handle)
        {
            _musicPlayer = new MusicPlayer(handle);

            // 填充音乐列表
            string audioPath = Application.StartupPath + "\\audio";
            string[] files = System.IO.Directory.GetFiles(audioPath);
            foreach (var file in files)
            {
                string filename = System.IO.Path.GetFileName(file);
                this.comboBoxMusicList.Items.Add(filename);
            }
            if (this.comboBoxMusicList.Items.Count > 0)
            {
                this.comboBoxMusicList.SelectedIndex = 0;
            }
        }

        private void buttonTestPlay_Click(object sender, EventArgs e)
        {
            if (this.buttonTestPlay.Text == "试听")
            {
                this.buttonTestPlay.Text = "停止";
                if (this.comboBoxMusicList.SelectedItem != null)
                {
                    string filename = this.comboBoxMusicList.SelectedItem.ToString();
                    string file = Application.StartupPath + "\\audio\\" + filename;
                    _musicPlayer.Play(file);
                }
            }
            else
            {
                this.buttonTestPlay.Text = "试听";
                _musicPlayer.Stop();
            }
        }

        public void Play()
        {
            this.buttonTestPlay.Text = "试听";
            buttonTestPlay_Click(null, null);
        }
    }
}
