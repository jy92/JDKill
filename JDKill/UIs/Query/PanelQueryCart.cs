﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JDKill.UIs.Common;
using TDLib.Utils;
using JDKill.Definitions.Cart;

namespace JDKill.UIs.Query
{
    public partial class PanelQueryCart : PanelBase
    {
        public PanelQueryCart()
        {
            InitializeComponent();
        }

        private void toolStripButtonQuery_Click(object sender, EventArgs e)
        {
            if (!GlobalContext.IsLogined())
                return;

            RunTaskThread(() => {
                try
                {
                    this.dataGridView1.Rows.Clear();
                    GlobalContext.DoOutput("正在查询购物车...");
                    var cartList = GlobalContext.KillService.QueryCartProductList();
                    for (int i = 0; i < cartList.Count; i++)
                    {
                        var cart = cartList[i];
                        this.dataGridView1.Rows.Add(new object[] { 
                            i+1,
                            cart.ProductId,
                            cart.ProductName,
                            cart.Count,
                            cart.Price,
                            cart.PriceSum,
                            cart.Checked
                        });
                        this.dataGridView1.Rows[i].Tag = cart;
                    }
                    GlobalContext.DoOutput("购物车中包含"+cartList.Count+"件商品！");
                }
                catch (Exception ex)
                {
                    GlobalContext.DoOutput("查询购物车失败，" + ex.Message);
                }
            });
        }

        private void dataGridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
                return;

            if (this.dataGridView1.SelectedRows.Count < 1)
            {
                MsgBox.Warning("请先选择商品！");
                return;
            }

            ContextMenuStrip cms = new ContextMenuStrip();
            var info = this.dataGridView1.SelectedRows[0].Tag as CartProductInfo;
            cms.Items.Add("移除购物车", null, (o, args) => {

                if (MsgBox.Question("确定要从购物车中删除商品：" + info.ProductName + "?", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                    return;

                RunTaskThread(() =>
                {
                    try
                    {
                        GlobalContext.KillService.RemoveCartProduct(info.ProductId);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.Error("删除商品失败，" + ex.Message);
                    }
                    toolStripButtonQuery_Click(null, null);
                });
            });
            cms.Items.Add("勾选商品", null, (o, args) =>
            {
                RunTaskThread(() =>
                {
                    try
                    {
                        GlobalContext.KillService.ChangeCartProductSelection(info,"",true);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.Error("勾选商品失败，" + ex.Message);
                    }
                    toolStripButtonQuery_Click(null, null);
                });
            });
            cms.Items.Add("取消勾选商品", null, (o, args) =>
            {
                RunTaskThread(() =>
                {
                    try
                    {
                        GlobalContext.KillService.ChangeCartProductSelection(info, "", false);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.Error("取消勾选商品失败，" + ex.Message);
                    }
                    toolStripButtonQuery_Click(null, null);
                });
            });
            cms.Items.Add("调整数量", null, (o, args) =>
            {
                RunTaskThread(() =>
                {
                    var frm = new FrmInputNumber();
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        try
                        {
                            GlobalContext.KillService.ChangeCartProductNum(info,frm.InputValue,"");
                        }
                        catch (Exception ex)
                        {
                            MsgBox.Error("调整数量失败，" + ex.Message);
                        }
                        toolStripButtonQuery_Click(null, null);
                    }
                });
            });
            cms.Show(this.dataGridView1, e.Location);
        }
    }
}
