﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JDKill.UIs.Common;
using TDLib.Utils;

namespace JDKill.UIs.Query
{
    public partial class FrmAddCustomProduct : FrmIconBase
    {
        public string ProductId { get; private set; }

        public FrmAddCustomProduct()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string url = this.textBox1.Text.Trim();
            string productId = GlobalContext.KillService.GetProductId(url);
            if (productId == null)
            {
                MsgBox.Warning("商品链接无效！");
                return;
            }
            this.ProductId = productId;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

    }
}
