﻿namespace JDKill.UIs.Query
{
    partial class PanelQueryProduct
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelQueryProduct));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listViewProductList = new TDLib.UIs.Controls.ListViewExNoBlink();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panelSeries = new System.Windows.Forms.Panel();
            this.checkBoxSeriesAll = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonAddCustom = new System.Windows.Forms.ToolStripButton();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panelSeries.SuspendLayout();
            this.panel2.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "剩余(件)";
            this.columnHeader9.Width = 110;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "是否可购";
            this.columnHeader4.Width = 80;
            // 
            // listViewProductList
            // 
            this.listViewProductList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader11,
            this.columnHeader6,
            this.columnHeader3,
            this.columnHeader7,
            this.columnHeader2,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader4});
            this.listViewProductList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewProductList.FullRowSelect = true;
            this.listViewProductList.GridLines = true;
            this.listViewProductList.Location = new System.Drawing.Point(0, 75);
            this.listViewProductList.Name = "listViewProductList";
            this.listViewProductList.Size = new System.Drawing.Size(961, 136);
            this.listViewProductList.TabIndex = 5;
            this.listViewProductList.UseCompatibleStateImageBehavior = false;
            this.listViewProductList.View = System.Windows.Forms.View.Details;
            this.listViewProductList.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listViewProductList_MouseUp);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "#";
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "种类";
            this.columnHeader11.Width = 100;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "商品编号";
            this.columnHeader6.Width = 122;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "商品名称";
            this.columnHeader3.Width = 366;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "商品价格";
            this.columnHeader7.Width = 110;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "已售出(件)";
            this.columnHeader8.Width = 110;
            // 
            // panelSeries
            // 
            this.panelSeries.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelSeries.Controls.Add(this.checkBoxSeriesAll);
            this.panelSeries.Location = new System.Drawing.Point(88, 10);
            this.panelSeries.Name = "panelSeries";
            this.panelSeries.Size = new System.Drawing.Size(442, 23);
            this.panelSeries.TabIndex = 5;
            // 
            // checkBoxSeriesAll
            // 
            this.checkBoxSeriesAll.AutoSize = true;
            this.checkBoxSeriesAll.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBoxSeriesAll.Location = new System.Drawing.Point(5, 3);
            this.checkBoxSeriesAll.Name = "checkBoxSeriesAll";
            this.checkBoxSeriesAll.Size = new System.Drawing.Size(48, 16);
            this.checkBoxSeriesAll.TabIndex = 4;
            this.checkBoxSeriesAll.Text = "全选";
            this.checkBoxSeriesAll.UseVisualStyleBackColor = true;
            this.checkBoxSeriesAll.CheckedChanged += new System.EventHandler(this.checkBoxSeriesAll_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.comboBox3);
            this.panel2.Controls.Add(this.comboBox2);
            this.panel2.Controls.Add(this.comboBox1);
            this.panel2.Controls.Add(this.panelSeries);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 25);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(961, 50);
            this.panel2.TabIndex = 4;
            // 
            // comboBox3
            // 
            this.comboBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(825, 13);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 20);
            this.comboBox3.TabIndex = 6;
            // 
            // comboBox2
            // 
            this.comboBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(688, 13);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 20);
            this.comboBox2.TabIndex = 6;
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(550, 13);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 20);
            this.comboBox1.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "筛选种类：";
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonAddCustom});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(961, 25);
            this.toolStrip2.TabIndex = 6;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripButtonAddCustom
            // 
            this.toolStripButtonAddCustom.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddCustom.Image")));
            this.toolStripButtonAddCustom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddCustom.Name = "toolStripButtonAddCustom";
            this.toolStripButtonAddCustom.Size = new System.Drawing.Size(112, 22);
            this.toolStripButtonAddCustom.Text = "添加自定义商品";
            this.toolStripButtonAddCustom.Click += new System.EventHandler(this.toolStripButtonAddCustom_Click);
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "秒杀价格";
            this.columnHeader2.Width = 110;
            // 
            // PanelQueryProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listViewProductList);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.toolStrip2);
            this.Name = "PanelQueryProduct";
            this.Size = new System.Drawing.Size(961, 211);
            this.Load += new System.EventHandler(this.QueryProductsPanel_Load);
            this.panelSeries.ResumeLayout(false);
            this.panelSeries.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private TDLib.UIs.Controls.ListViewExNoBlink listViewProductList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Panel panelSeries;
        private System.Windows.Forms.CheckBox checkBoxSeriesAll;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddCustom;
        private System.Windows.Forms.ColumnHeader columnHeader2;
    }
}
