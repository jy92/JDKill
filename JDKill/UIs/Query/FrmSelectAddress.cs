﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TDLib.UIs.Forms;
using JDKill.Definitions;
using JDKill.Services;
using TDLib.Utils;
using JDKill.Definitions.Address;
using JDKill.UIs.Common;

namespace JDKill.UIs.Query
{
    public partial class FrmSelectAddress : FrmIconBase
    {
        public AddressDetail SelectedAddress { get; private set; }
        public FrmSelectAddress()
        {
            InitializeComponent();
        }

        private void FrmQueryAddress_Load(object sender, EventArgs e)
        {
            RunTaskThread(() =>
            {
                this.dataGridView1.Rows.Clear();
                try
                {
                    var result = GlobalContext.KillService.QueryAddressList();
                    for (int i = 0; i < result.Count; i++)
                    {
                        var address = result[i];
                        this.dataGridView1.Rows.Add(new object[]{
                            (i+1).ToString(),
                            address.Id,
                            address.ReceiverName,
                            address.ReceiverMobile,
                            address.FullAddress
                        });
                        this.dataGridView1.Rows[i].Tag = address;
                    }
                }
                catch (Exception ex)
                {
                    MsgBox.Warning("加载收货地址失败," + ex.Message);
                }
            });
        }

        private void dataGridView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.dataGridView1.SelectedRows.Count > 0)
            {
                var address = this.dataGridView1.SelectedRows[0].Tag as AddressDetail;
                this.SelectedAddress = address;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }
    }
}
