﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JDKill.UIs.Common;
using TDLib.Utils;

namespace JDKill.UIs.Query
{
    public partial class FrmAddCommonUrl : FrmIconBase
    {
        public string UrlName { get; private set; }
        public string UrlValue { get; private set; }

        public FrmAddCommonUrl()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string name = this.textBox1.Text.Trim();
            if (name.Length < 1)
            {
                MsgBox.Warning("别名不能为空！");
                return;
            }
            string url = this.textBox2.Text.Trim();
            if (url.Length < 1)
            {
                MsgBox.Warning("链接不能为空！");
                return;
            }
            this.UrlName = name;
            this.UrlValue = url;
            this.DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
