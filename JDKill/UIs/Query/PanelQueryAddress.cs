﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JDKill.UIs.Common;

namespace JDKill.UIs.Query
{
    public partial class PanelQueryAddress : PanelBase
    {
        public PanelQueryAddress()
        {
            InitializeComponent();
        }

        private void toolStripButtonQueryAddress_Click(object sender, EventArgs e)
        {
            if (!GlobalContext.IsLogined())
                return;

            RunTaskThread(() =>
            {
                this.dataGridView1.Rows.Clear();
                GlobalContext.DoOutput("查询收货地址...");
                try
                {
                    var result = GlobalContext.KillService.QueryAddressList();
                    for (int i = 0; i < result.Count; i++)
                    {
                        var address = result[i];
                        this.dataGridView1.Rows.Add(new object[]{
                            (i+1).ToString(),
                            address.Id,
                            address.ReceiverName,
                            address.ReceiverMobile,
                            address.FullAddress
                        });
                        this.dataGridView1.Rows[i].Tag = address;
                    }
                    GlobalContext.DoOutput("查询收货地址成功！");
                }
                catch (Exception ex)
                {
                    GlobalContext.DoOutput("查询收货地址失败," + ex.Message);
                }
            });
        }
    }
}
