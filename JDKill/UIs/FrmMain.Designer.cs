﻿namespace JDKill.UIs
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panelQueryProduct1 = new JDKill.UIs.Query.PanelQueryProduct();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panelQueryAddress1 = new JDKill.UIs.Query.PanelQueryAddress();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panelProductKillSetting1 = new JDKill.UIs.Setting.PanelProductKillSetting();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panelQQNotifierSetting1 = new JDKill.UIs.Setting.PanelQQNotifierSetting();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panelAudioNotifierSetting1 = new JDKill.UIs.Setting.PanelAudioNotifierSetting();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panelUniversalSetting1 = new JDKill.UIs.Setting.PanelUniversalSetting();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonLoginOrLogout = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonQuery = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonMonitor = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCommonUrl = new System.Windows.Forms.ToolStripSplitButton();
            this.添加常用地址ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.visualStyler1 = new SkinSoft.VisualStyler.VisualStyler(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visualStyler1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 39);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1043, 338);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panelQueryProduct1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1035, 312);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "抢购页面";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panelQueryProduct1
            // 
            this.panelQueryProduct1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueryProduct1.Location = new System.Drawing.Point(3, 3);
            this.panelQueryProduct1.Name = "panelQueryProduct1";
            this.panelQueryProduct1.Size = new System.Drawing.Size(1029, 306);
            this.panelQueryProduct1.TabIndex = 0;
            this.panelQueryProduct1.AddToProductKillList += new System.EventHandler<JDKill.UIs.Query.AddToProductKillListEventArgs>(this.panelQueryProduct1_AddToProductKillList);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panelQueryAddress1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1035, 312);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "收货地址";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panelQueryAddress1
            // 
            this.panelQueryAddress1.BackColor = System.Drawing.Color.White;
            this.panelQueryAddress1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueryAddress1.Location = new System.Drawing.Point(0, 0);
            this.panelQueryAddress1.Name = "panelQueryAddress1";
            this.panelQueryAddress1.Size = new System.Drawing.Size(1035, 312);
            this.panelQueryAddress1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.tabControl2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 377);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1043, 206);
            this.panel1.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.textBoxOutput);
            this.groupBox1.Location = new System.Drawing.Point(597, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(441, 194);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "输出";
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.BackColor = System.Drawing.Color.White;
            this.textBoxOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxOutput.Location = new System.Drawing.Point(3, 17);
            this.textBoxOutput.Multiline = true;
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.ReadOnly = true;
            this.textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxOutput.Size = new System.Drawing.Size(435, 174);
            this.textBoxOutput.TabIndex = 0;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Location = new System.Drawing.Point(6, 8);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(585, 191);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panelProductKillSetting1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(577, 165);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "抢购设置";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panelProductKillSetting1
            // 
            this.panelProductKillSetting1.BackColor = System.Drawing.Color.White;
            this.panelProductKillSetting1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelProductKillSetting1.Location = new System.Drawing.Point(3, 3);
            this.panelProductKillSetting1.Name = "panelProductKillSetting1";
            this.panelProductKillSetting1.Size = new System.Drawing.Size(571, 159);
            this.panelProductKillSetting1.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.panelQQNotifierSetting1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(577, 165);
            this.tabPage5.TabIndex = 3;
            this.tabPage5.Text = "QQ通知";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // panelQQNotifierSetting1
            // 
            this.panelQQNotifierSetting1.BackColor = System.Drawing.Color.White;
            this.panelQQNotifierSetting1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQQNotifierSetting1.Location = new System.Drawing.Point(0, 0);
            this.panelQQNotifierSetting1.Name = "panelQQNotifierSetting1";
            this.panelQQNotifierSetting1.Size = new System.Drawing.Size(577, 165);
            this.panelQQNotifierSetting1.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.panelAudioNotifierSetting1);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(577, 165);
            this.tabPage6.TabIndex = 4;
            this.tabPage6.Text = "声音提醒";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // panelAudioNotifierSetting1
            // 
            this.panelAudioNotifierSetting1.BackColor = System.Drawing.Color.White;
            this.panelAudioNotifierSetting1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAudioNotifierSetting1.Location = new System.Drawing.Point(0, 0);
            this.panelAudioNotifierSetting1.Name = "panelAudioNotifierSetting1";
            this.panelAudioNotifierSetting1.Size = new System.Drawing.Size(577, 165);
            this.panelAudioNotifierSetting1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panelUniversalSetting1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(577, 165);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "系统设置";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panelUniversalSetting1
            // 
            this.panelUniversalSetting1.BackColor = System.Drawing.Color.White;
            this.panelUniversalSetting1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUniversalSetting1.Location = new System.Drawing.Point(3, 3);
            this.panelUniversalSetting1.Name = "panelUniversalSetting1";
            this.panelUniversalSetting1.Size = new System.Drawing.Size(571, 159);
            this.panelUniversalSetting1.TabIndex = 0;
            this.panelUniversalSetting1.OnTimerKillEnabled += new System.EventHandler(this.panelUniversalSetting1_OnTimerKillEnabled);
            this.panelUniversalSetting1.OnTimerKillDisabled += new System.EventHandler(this.panelUniversalSetting1_OnTimerKillDisabled);
            this.panelUniversalSetting1.OnTimerKillTrigger += new System.EventHandler(this.panelUniversalSetting1_OnTimerKillTrigger);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonLoginOrLogout,
            this.toolStripButtonQuery,
            this.toolStripButtonMonitor,
            this.toolStripButtonCommonUrl});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1043, 39);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonLoginOrLogout
            // 
            this.toolStripButtonLoginOrLogout.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLoginOrLogout.Image")));
            this.toolStripButtonLoginOrLogout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLoginOrLogout.Name = "toolStripButtonLoginOrLogout";
            this.toolStripButtonLoginOrLogout.Size = new System.Drawing.Size(97, 36);
            this.toolStripButtonLoginOrLogout.Text = "登陆/注销";
            this.toolStripButtonLoginOrLogout.Click += new System.EventHandler(this.toolStripButtonLoginOrLogout_Click);
            // 
            // toolStripButtonQuery
            // 
            this.toolStripButtonQuery.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonQuery.Image")));
            this.toolStripButtonQuery.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonQuery.Name = "toolStripButtonQuery";
            this.toolStripButtonQuery.Size = new System.Drawing.Size(68, 36);
            this.toolStripButtonQuery.Text = "查询";
            this.toolStripButtonQuery.Click += new System.EventHandler(this.toolStripButtonQuery_Click);
            // 
            // toolStripButtonMonitor
            // 
            this.toolStripButtonMonitor.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonMonitor.Image")));
            this.toolStripButtonMonitor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMonitor.Name = "toolStripButtonMonitor";
            this.toolStripButtonMonitor.Size = new System.Drawing.Size(92, 36);
            this.toolStripButtonMonitor.Text = "开始抢购";
            this.toolStripButtonMonitor.Click += new System.EventHandler(this.toolStripButtonMonitor_Click);
            // 
            // toolStripButtonCommonUrl
            // 
            this.toolStripButtonCommonUrl.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加常用地址ToolStripMenuItem});
            this.toolStripButtonCommonUrl.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCommonUrl.Image")));
            this.toolStripButtonCommonUrl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCommonUrl.Name = "toolStripButtonCommonUrl";
            this.toolStripButtonCommonUrl.Size = new System.Drawing.Size(104, 36);
            this.toolStripButtonCommonUrl.Text = "常用链接";
            this.toolStripButtonCommonUrl.ButtonClick += new System.EventHandler(this.toolStripButtonCommonUrl_ButtonClick);
            // 
            // 添加常用地址ToolStripMenuItem
            // 
            this.添加常用地址ToolStripMenuItem.Name = "添加常用地址ToolStripMenuItem";
            this.添加常用地址ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.添加常用地址ToolStripMenuItem.Text = "添加常用链接";
            this.添加常用地址ToolStripMenuItem.Click += new System.EventHandler(this.添加常用地址ToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 583);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1043, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(32, 17);
            this.toolStripStatusLabel1.Text = "用户";
            // 
            // visualStyler1
            // 
            this.visualStyler1.HostForm = null;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 605);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "FrmMain";
            this.Text = "京东秒杀助手";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.SizeChanged += new System.EventHandler(this.FrmMain_SizeChanged);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visualStyler1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButtonQuery;
        private System.Windows.Forms.ToolStripButton toolStripButtonMonitor;
        private SkinSoft.VisualStyler.VisualStyler visualStyler1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ToolStripButton toolStripButtonLoginOrLogout;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private Query.PanelQueryProduct panelQueryProduct1;
        private Query.PanelQueryAddress panelQueryAddress1;
        private Setting.PanelQQNotifierSetting panelQQNotifierSetting1;
        private Setting.PanelAudioNotifierSetting panelAudioNotifierSetting1;
        private Setting.PanelUniversalSetting panelUniversalSetting1;
        private Setting.PanelProductKillSetting panelProductKillSetting1;
        private System.Windows.Forms.ToolStripSplitButton toolStripButtonCommonUrl;
        private System.Windows.Forms.ToolStripMenuItem 添加常用地址ToolStripMenuItem;

    }
}

