﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TDLib.Utils;

namespace JDKill
{
    class ConfigStorage
    {
        public static List<string> ReadCustomProducts()
        {
            string str = AppSettingsUtil.GetValue("customProducts", string.Empty);
            if(str.Trim().Length<1)
                return new List<string>();

            return str.Split('_').ToList();
        }

        public static void SaveCustomProducts(List<string> customProducts)
        {
            AppSettingsUtil.SetValue("customProducts", string.Join("_",customProducts.ToArray()));
        }

        public static string ReadLocationName()
        {
            return AppSettingsUtil.GetValue("locationName", null);
        }

        public static void SaveLocationName(string locationName)
        {
            AppSettingsUtil.SetValue("locationName", locationName);
        }

        public static string ReadLocationId()
        {
            return AppSettingsUtil.GetValue("locationId", null);
        }

        public static void SaveLocationId(string locationId)
        {
            AppSettingsUtil.SetValue("locationId", locationId);
        }

        public static void SaveUser(string userId, string cookie)
        {
            TDLib.Utils.AppSettingsUtil.SetValue("user_"+userId, cookie);
        }

        public static string ReadUserCookie(string userId)
        {
            return TDLib.Utils.AppSettingsUtil.GetValue("user_" + userId, null);
        }

        public static List<string> ReadAllUsers()
        {
            List<string> list = new List<string>();
            var keys = TDLib.Utils.AppSettingsUtil.GetKeys();
            foreach (var key in keys)
            {
                if (key.StartsWith("user_"))
                {
                    string mobile = key.Remove(0, "user_".Length);
                    list.Add(mobile);
                }
            }
            return list;
        }

        public static void SaveCommonUrl(string name, string url)
        {
            TDLib.Utils.AppSettingsUtil.SetValue("commonUrl_" + name, url);
        }

        public static Dictionary<string,string> ReadAllCommonUrl()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            var keys = TDLib.Utils.AppSettingsUtil.GetKeys();
            foreach (var key in keys)
            {
                if (key.StartsWith("commonUrl_"))
                {
                    string name = key.Remove(0, "commonUrl_".Length);
                    string url = AppSettingsUtil.GetValue(key, null);
                    if (!dic.ContainsKey(name))
                    {
                        dic.Add(name,url);
                    }
                }
            }
            return dic;
        }
    }
}
