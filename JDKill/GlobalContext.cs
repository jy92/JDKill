﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using JDKill.Services;
using JDKill.UIs;
using TDLib.Utils;

namespace JDKill
{
    class GlobalContext
    {
        private static bool _isLogined = false;
        public static FrmMain MainForm = null;
        public static Color OnSaleColor = Color.Yellow;
        public static List<string> CustomProducts = null;
        public static JDService KillService = new JDService();

        static GlobalContext()
        {
            CustomProducts = ConfigStorage.ReadCustomProducts();
        }

        public static void DoOutput(string str)
        {
            if (MainForm != null)
            {
                MainForm.DoOutput(str);
            }
        }

        public static bool IsLogined()
        {
            if (!_isLogined)
            {
                MsgBox.Warning("请先登录！");
            }
            return _isLogined;
        }

        public static bool IsInProductKillProcess
        {
            get { return MainForm._isMonitorRunning; }
        }

        public static void SetLoginState(bool isLogined)
        {
            _isLogined = isLogined;
        }
    }
}
