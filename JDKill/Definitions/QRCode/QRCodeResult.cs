﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JDKill.Definitions.QRCode
{
    public class QRCodeResult
    {
        public int code;
        public string msg;
        public string ticket;
    }
}
