﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JDKill.Definitions.User
{
    public class UserInfoDetail
    {
        public string Id { get { return this.LoginName; } }
        public string UserName;
        public string LoginName;
        public string NickName;
        public string Mobile;
        public string Email;
    }
}
