﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace JDKill.Definitions.User
{
    public class UserInfoBrief
    {
        [JsonProperty("realName")]
        public string UserName;
        [JsonProperty("nickName")]
        public string NickName;
        [JsonProperty("userLevel")]
        public string UserLevel;
        [JsonProperty("imgUrl")]
        public string Avatar;
    }
}
