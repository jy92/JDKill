﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JDKill.Definitions.Address
{
    public class AreaInfo
    {
        public string id;
        public string name;
        public override string ToString()
        {
            return name;
        }
    }
}
