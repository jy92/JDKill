﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace JDKill.Definitions.Address
{
    public class AddressDetail
    {
        [JsonProperty("id")]
        public string Id;
        [JsonProperty("name")]
        public string ReceiverName;
        [JsonProperty("mobile")]
        public string ReceiverMobile;
        [JsonProperty("fullAddress")]
        public string FullAddress;
        [JsonProperty("areaName")]
        public string AreaName;
        [JsonProperty("addressDetail")]
        public string AreaDetail;
        [JsonProperty("addressDefault")]
        public bool IsDefault;
        [JsonProperty("provinceId")]
        public string ProvinceId;
        [JsonProperty("cityId")]
        public string CityId;
        [JsonProperty("countyId")]
        public string CountyId;
        public string GetLocationId()
        {
            return this.ProvinceId + "_" + this.CityId + "_" + this.CountyId;
        }
        public override string ToString()
        {
            return string.Format("{0},{1},{2}",
                this.ReceiverName, this.ReceiverMobile, this.FullAddress);
        }
    }
}
