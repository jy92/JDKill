﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JDKill.Definitions
{
    /// <summary>
    /// 下单条件
    /// </summary>
    public class MakeOrderCondition : AbstractType
    {
        private MakeOrderCondition(string id, string name):base(id,name)
        {
            
        }

        /// <summary>
        /// 有货
        /// </summary>
        public static readonly MakeOrderCondition WhenAvailable = new MakeOrderCondition("1", "有货就下单");

        /// <summary>
        /// 有货且小于等于指定价格
        /// </summary>
        public static readonly MakeOrderCondition WhenAvailableAndLessEqualCertainPrice = new MakeOrderCondition("1", "有货且小于等于指定价格");

        /// <summary>
        /// 所有定义的LineSetSplitOperationType
        /// </summary>
        public static readonly List<MakeOrderCondition> AllDefinedTypes = new List<MakeOrderCondition>()
        {
            WhenAvailable,
            WhenAvailableAndLessEqualCertainPrice
        };
    }
}
