﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JDKill.Definitions.Product
{
    public class ProductDetail
    {
        public string Id;
        public string Name;
        public string CartUrl;
        public double Price;
        public ProductState State;
        public string SaleCount = "-";
        public string LeftCount = "-";
        public List<string> Images;
        public double KillPrice;

        public bool IsBuyAvailable()
        {
            return this.State == ProductState.Available;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
