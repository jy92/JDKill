﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JDKill.Definitions.Product
{
    class ProductKillInfo
    {
        public int Index;
        public string AreaId;
        public string GoodsId;
        public override bool Equals(object obj)
        {
            ProductKillInfo other = obj as ProductKillInfo;
            return this.AreaId == other.AreaId && this.GoodsId == other.GoodsId;
        }
    }
}
