﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JDKill.Definitions.Product
{
    public enum ProductState
    {
        /// <summary>
        /// 有货
        /// </summary>
        Available = 33,
        /// <summary>
        /// 无货
        /// </summary>
        Empty = 34
    }
}
