﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JDKill.Definitions.Product
{
    public class ProductCategory
    {
        public string Id { get; private set; }
        public string Name { get; private set; }
        public bool IsKill { get; private set; }

        public ProductCategory(string id, string name) : this(id,name,false)
        {

        }

        public ProductCategory(string id, string name, bool isKill)
        {
            this.Id = id;
            this.Name = name;
            this.IsKill = isKill;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            var other = obj as ProductCategory;
            if (other == null)
                return false;

            return this.Id == other.Id;
        }

        public static readonly ProductCategory Custom = new ProductCategory("1", "自定义");
        public static readonly List<ProductCategory> AllCategory = new List<ProductCategory>();

        static ProductCategory()
        {
            AllCategory = new List<ProductCategory>() { 
               Custom
            };
        }
    }
}
