﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JDKill.Definitions
{
    /// <summary>
    /// 抽象类型
    /// </summary>
    public abstract class AbstractType
    {
        /// <summary>
        /// 编号，作为唯一标识
        /// </summary>
        public string Id { get; private set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; private set; }

        protected AbstractType(string id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        /// <summary>
        /// 重写ToString函数，用于显示对象的名称
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// 重写Equals函数，用于判断两个对象是否相等
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            AbstractType other = obj as AbstractType;
            if (other == null)
                return false;
            return this.Id == other.Id;
        }
    }
}
