﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JDKill.Definitions.Cart
{
    /// <summary>
    /// 购物车商品信息
    /// </summary>
    public class CartProductInfo
    {
        public bool Checked;
        public string ProductId;
        public string ProductType;
        public string PromoId;
        public string ProductName;
        public int Count;
        public double Price;
        public double PriceSum;
    }
}
